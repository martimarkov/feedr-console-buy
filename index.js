'use strict';

const puppeteer = require('puppeteer');
const config = require('config');
const fs = require('fs');
const query = require('cli-interact').getYesNo;

const SUBSIDY_SUMMARY_SELECTOR = 'div[class^=SubsidySummary]';
const MENU_SELECTOR = 'div[class^=MenuGrid__Wrapper]';
const MENU_ITEM_TEXT_SELECTOR = 'div[class^=MenuItem__Copy]';
const ORDER_BUTTON = 'button[class^=OrderBar__OrderButton]';
const MODAL_ORDER_BUTTON =
  '#modal-root *[class^="Button__Wrapper"] button[class^="fdr-input btn-primary"]';
const MODAL_ORDER_CONFIRMATION =
  '#modal-root *[class^=ConfirmationModal__Wrapper]';
const MODAL_CONFIRM_BUTTON =
  'div#modal-root button[class^="fdr-input btn-primary MenuModal__Confirm"]';
const MODAL_CLOSE_BUTTON = '.fdr-modal-close';
const MODAL_ITEM_DESCRIPTION =
  '#modal-root div[class^=MenuModal__Copy] p:nth-child(3)';
const MODAL_ITEM_TITLE = '#modal-root div[class^=MenuModal__Copy] h2';
const MODAL_MINUS_BUTTON = 'div#modal-root button[class^=number-step-minus]';
const MODAL_PLUS_BUTTON = 'div#modal-root button[class^=number-step-plus]';

const WEEKDAY_NUMBERS = { Thursday: 4, Friday: 5 };
const FRIDAY_WEEKDAY_NUMBER = 5;
let totalCredits = 0;

const bot = async () => {
  const browser = await puppeteer.launch({
    headless: config.get('app.headless'),
    devtools: true,
  });
  const page = await browser.newPage();

  await page.goto('https://teamfeedr.com/#login', { waitUntil: 'load' });
  //   await page.waitFor(3000);
  console.log('Logging in...');
  await page.type('input#login-email', config.get('feedr.username'));
  await page.type('input#login-password', config.get('feedr.password'));
  await page.click('div#modal-root form button');
  await page.waitForNavigation();
  await page.waitForSelector(SUBSIDY_SUMMARY_SELECTOR);
  console.log('Logged in successfully.');

  //Because Feedr's code has issues with saving to Local Storage
  await page.waitFor(30);

  const days = config.get('app.days');
  console.log('Your selected days', days);

  for (let i = 0; i < days.length; i++) {
    const day = days[i];
    if (WEEKDAY_NUMBERS.hasOwnProperty(day)) {
      console.log(`Now processing day ${day}`);
      const boughtItems = await dayBuy(WEEKDAY_NUMBERS[day], page);
      console.log(`Bought items for ${day}:\n`);
      boughtItems.forEach(item => {
        console.log(item.title + '\n');
      });
    } else {
      console.log('Hmm... Strange... That day seems to not exist.');
    }
  }

  console.log(
    'You should now receive emails from Feedr confirming your items.\n' +
      "If you do not then the script MIGHT HAVE FAILED. You'll need to check your account.",
  );
  await browser.close();
};

const buyItem = async (item, page) => {
  item.amount = 1;
  console.log(`Trying to add ${item.title}`);
  if (item.creditsCost < totalCredits) {
    await page.evaluate(
      ({ MENU_SELECTOR, item }) => {
        document
          .querySelectorAll(MENU_SELECTOR)
          [item.menu].children[item.index].click();
      },
      {
        MENU_SELECTOR,
        item,
      },
    );

    await page.waitFor(30);

    for (let i = 1; i < item.amount; i++) {
      console.log('Adding more of the same item');
      await page.click(MODAL_PLUS_BUTTON);
    }
    await page.click(MODAL_CONFIRM_BUTTON);
    await page.waitFor(50);
    await page.click(ORDER_BUTTON);
    await page.waitFor(50);
    await page.click(MODAL_ORDER_BUTTON);
    await page.waitForSelector(MODAL_ORDER_CONFIRMATION);
    totalCredits -= item.creditsCost;
    return true;
  } else {
    console.log('Not enough credits. This script WILL NOT SUPPORT auto topup.');
    return false;
  }
};

const dayBuy = async (selectedDay, page) => {
  totalCredits = 0;
  const boughtItems = [];
  const nextDate = new Date();
  nextDate.setDate(
    nextDate.getDate() + ((selectedDay + 7 - nextDate.getDay()) % 7),
  );
  const day = nextDate.getDate();
  const month = nextDate.getMonth() + 1; //Months are zero based
  const year = nextDate.getFullYear();
  await page.goto(
    `https://teamfeedr.com/canteen?date=${year}-${month}-${day}&meal=lunch`,
    {
      waitUntil: 'load',
    },
  );
  await page.waitForSelector(SUBSIDY_SUMMARY_SELECTOR);
  const { freeCreditsText, paidCreditsText } = await page.evaluate(
    ({ SUBSIDY_SUMMARY_SELECTOR }) => {
      var freeCreditsText = document.querySelector(
        SUBSIDY_SUMMARY_SELECTOR + ' h3',
      ).textContent;
      var paidCreditsText = document.querySelector(
        SUBSIDY_SUMMARY_SELECTOR + ' p',
      ).textContent;
      return {
        freeCreditsText: freeCreditsText,
        paidCreditsText: paidCreditsText,
      };
    },
    {
      SUBSIDY_SUMMARY_SELECTOR,
    },
  );
  const freeCredits = parseFloat(/([\d]+[.][\d]+)/g.exec(freeCreditsText)[0]);
  const paidCredits = parseFloat(/([\d]+[.][\d]+)/g.exec(paidCreditsText)[0]);
  totalCredits = parseFloat(freeCredits) + parseFloat(paidCredits);
  console.log(`Free credits for the day ${freeCredits}`);
  console.log(`Paid credits for the day ${paidCredits}`);
  console.log(`Total credits for the day ${totalCredits}`);

  const menuLoaded = await page
    .waitForSelector(MENU_SELECTOR, { timeout: 10000 })
    .then(() => true)
    .catch(error => {
      console.log(
        "The loading of the menu timed out. Most likely there isn't anything to select on that day",
      );
      return false;
    });

  if (!menuLoaded) {
    return [];
  }

  const { mainsMenuItems, sidesMenuItems } = await page.evaluate(
    ({
      MENU_SELECTOR,
      MENU_ITEM_TEXT_SELECTOR,
      MODAL_MINUS_BUTTON,
      MODAL_CLOSE_BUTTON,
      MODAL_ITEM_TITLE,
      MODAL_ITEM_DESCRIPTION,
    }) => {
      const extractMenuItems = (menu, menuItems) => {
        const itemsArray = [];
        const totalItems = menuItems.length;
        for (let index = 0; index < totalItems; index++) {
          const currentItem = menuItems[index];

          const itemDetails = currentItem.querySelector(
            MENU_ITEM_TEXT_SELECTOR,
          );

          const creditsCostText = itemDetails.querySelector('span.credits')
            .textContent;
          const creditsCost = /([\d]+[.][\d]+)/g.exec(creditsCostText)[0];

          const dietaryDetailsHTML = itemDetails.querySelector('span.dietaries')
            .children;

          const dietaryDetails = [];
          const dietaryDetailsTotalItems = dietaryDetailsHTML.length;
          for (let j = 0; j < dietaryDetailsTotalItems; j++) {
            dietaryDetails.push(dietaryDetailsHTML[j].innerText);
          }

          // Open modal to get the description
          document
            .querySelectorAll(MENU_SELECTOR)
            [menu].children[index].click();
          const description = document.querySelector(MODAL_ITEM_DESCRIPTION)
            .textContent;
          const title = document.querySelector(MODAL_ITEM_TITLE).textContent;

          document.querySelector(MODAL_CLOSE_BUTTON).click();

          itemsArray.push({
            index: index,
            menu: menu,
            title: title,
            description: description,
            creditsCost: parseFloat(creditsCost),
            dietaryDetails: dietaryDetails,
          });
        }
        return itemsArray;
      };
      var menus = document.querySelectorAll(MENU_SELECTOR);
      mainsMenu = menus[0].children;
      sidesMenu = menus[1].children;
      mainsMenuItems = extractMenuItems(0, mainsMenu);
      sidesMenuItems = extractMenuItems(1, sidesMenu);
      return {
        mainsMenuItems: mainsMenuItems,
        sidesMenuItems: sidesMenuItems,
      };
    },
    {
      MENU_SELECTOR,
      MENU_ITEM_TEXT_SELECTOR,
      MODAL_MINUS_BUTTON,
      MODAL_CLOSE_BUTTON,
      MODAL_ITEM_TITLE,
      MODAL_ITEM_DESCRIPTION,
    },
  );

  const totalMenuItems = mainsMenuItems.concat(sidesMenuItems);

  // Logic to select items

  console.log(`You have ${totalCredits} total credits`);
  if (config.get('app.onlyFree')) {
    console.log('Showing only free items (within free credits limit)');
  }
  for (let i = 0; i < totalMenuItems.length; i++) {
    const item = totalMenuItems[i];
    if (config.get('app.onlyFree') && item.creditsCost > totalCredits) {
      continue;
    }
    if (
      config
        .get('app.diateryRequirement')
        .some(r => item.dietaryDetails.includes(r))
    ) {
      console.log(
        `\nTitle: ${item.title}\n\nDescription:\n${item.description}\n\nCost: ${
          item.creditsCost
        } credits\n`,
      );
      const answer = query(`Do you want to add this item?`);

      if (answer) {
        console.log(`Buying ${item.title}`);
        const status = await buyItem(item, page);
        if (status) {
          boughtItems.push(item);
        }
        console.log(`You have ${totalCredits} credits left`);
      }
    }
  }

  return boughtItems;
};

bot();
