# Feedr Console Buy

## Description

The main reason behind the project is to allow the ordering to be done automtically based on user's preferences. So this way you won't be stuck without lunch even thou it shouldn't be that hard to have a flag in your account.

## Setup

1. Run `npm install`
2. Edit the config/default.json file with your preferences
3. Run `node index.js`

## Limits

1. No buying of credits. (out of scope)
1. You'll have to manually select your items.

## Roadmap

1. Automatic selection of a random item based on user's preferences.
1. List items that have already been ordered for each day.
